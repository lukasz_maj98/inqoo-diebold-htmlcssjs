class Product {
    constructor(id, name, price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }
}
class ProductsView {
    constructor(products, selector) {
        this.filtered = products;
        this.products = products;
        this.el = document.querySelector(selector);
    }
    show() {
        this.el.innerHTML = '';
        const div = document.createElement('div')
        this.filtered.forEach(element => {
            div.innerHTML = this.renderItem(element);
            const li = div.firstElementChild;
            this.el.appendChild(li);
        });
    }
    renderItem(item) {
        return `<div class="list-group-item d-flex justify-content-between" id="product_${item.id}">
                        <input type="text" id="product_value_${item.id}" value="" hidden>
                        <div class="d-flex flex-column">
                            <h3>${item.name}</h3>
                        <p>test</p>
                        </div>
                        <div class="d-flex-column">
                            <div class="text-center my-2 fw-bold">${item.price} USD</div>
                        </div>
                    </div>`;
    }
    push(product) {
        this.products.push(product);
        return products;
    }
    removeByID(itemID) {
        function ifindProd(item) {
            return item.id != itemID;
        }
        let filtered = this.products.filter(ifindProd)
        this.products = filtered;
        return products.show();
    }
    filterByPrice(itemPrice) {
        function priceFilter(item) {
            return item.price >= itemPrice;
        }
        this.filtered = this.products.filter(priceFilter)
        return this.show()
    }
    removeByName(itemName) {
        function nameRemove(item) {
            return item.name != itemName;
        }
        this.filtered = this.products.filter(nameRemove)
        return this.show()
    }
    filterByName(itemName) {
        function nameFilter(item) {
            return item.name == itemName;
        }
        this.filtered = this.products.filter(nameFilter)
        return this.show()
    }
    clearList() {
        this.filtered.splice(0);
        return this.show();
    }
}




var products = new ProductsView([], '.products_list');
var product1 = new Product(1, "Bułka", 1);
var product2 = new Product(2, "Mleko", 5);
var product3 = new Product(3, "Chleb", 10);



products.push(product1);
products.push(product2);
products.push(product3);
products.show();