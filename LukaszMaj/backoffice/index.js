class View {

  /** @type HTMLElement */
  el

  /**
   * Create view
   * @param { string | DOMElement } [el] 
   */
  constructor(el = document.createElement('div')) {
    this.el = typeof el === 'string' ? document.querySelector(el) : el;
    if (!this.el) { throw new Error('View mount element not found ' + el) }
  }

  listenTo(model) {
    this.model = model
    this.model.addEventListener('model_changed', () => {
      this.render()
    })
  }

}

class ProductsView extends View {
  /** @type ProductsModel */ model = null

  constructor(el) {
    super(el);
    this.el.addEventListener('click', e => this.handleDelete(e))
    this.el.addEventListener('click', e => this.handleClick(e))
  }
  handleDelete(event) {
    var btn = event.target.closest('.js-delete-product')
    if (!btn) { return }

    const item = event.target.closest('[data-product-id]')
    if (!item) { return }
    event.stopImmediatePropagation() // Prevent Select handler from firing!
    const product_id = item.dataset.productId
    // this.model.deleteProduct(product_id)
    console.log('Delete', product_id);
  }
  /**
   * Select product by DOM click
   * @param {MouseEvent} event 
   */
  handleClick(event) {
    const item = event.target.closest('[data-product-id]')
    if (!item) { return }
    const product_id = item.dataset.productId
    const product = this.model.findById(product_id)
    console.log(product_id, product);

    this.el.querySelectorAll('[data-product-id]').forEach(item => {
      item.classList.toggle('active', item.dataset.productId === product_id)
    })
  }

  render() {
    this.el.innerHTML = ''
    this.model.getItems().forEach(product => {
      const productView = new ProductView()
      productView.model = { product }
      this.el.append(productView.el)
      productView.render()
    })
  }
}

/** 
 * @typedef ProductViewModel
 * @property {Product} product
 * @property {boolean} selected
 */
class ProductView extends View {
  /** @type ProductViewModel */  model

  constructor(el) { super(el) }

  render() {
    const { product } = this.model
    this.el.outerHTML = /* html */`
    <div tabindex="1" class="list-group-item" 
        data-product-id="${product.id}">
      <div>
        <span>${product.name} </span>
        <span class="float-end">${(product.price_nett / 100).toFixed(2)} </span>
      </div>
      <div>
        <span class="float-end">
          <button class="js-delete-product">Delete</button>
        </span>
      </div>
    </div > `
  }
}

class ApplicationController {

  productsModel = new ProductsModel(produts)
  productsView = new ProductsView('#productListEl')
  productsSearchInput = document.querySelector('#productsSearchInput')
  /*
    productEditor = new ProductView('#product-editor')
  */
  constructor() {
    this.productsView.listenTo(this.productsModel)
    this.productsView.render()

    this.productsSearchInput.addEventListener('input', (event) => {
      const query = event.currentTarget.value
      this.productsModel.filter(query)
    })
  }

}

xhr = new XMLHttpRequest
xhr.open('GET', 'products.json')
xhr.addEventListener('loadend', event => {
    const data = JSON.parce(event.target.reposnseText)
    console.log(data)
})
xhr.send()


const app = new ApplicationController()
/*
app.productEditor.setData(products[0])
*/