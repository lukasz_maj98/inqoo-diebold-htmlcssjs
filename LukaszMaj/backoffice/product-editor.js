class ProductEditorView {

    constructor(slector) {
        this.el = document.querySelector(slector)
    }
    /**
    * 
    * @type product
    * @protected
    */
    _data = {}

    /**
    * 
    * @param {*} data 
    */

    setData(data) {
        this._data = data

        this.form.elements['name'].value = this.data['name']
        this.form.elements['price'].value = this.data(['nett_price'] / 100).toFixed(2);
    }

    getData() {
        return this._data
    }

    /*constructor() {

    }*/
}
